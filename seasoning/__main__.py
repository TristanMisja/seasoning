#!/usr/lib/Python3

import pwd
import spwd
import crypt
from __init__ import getpass
from hmac import compare_digest

def login():
    username = input("Username: ")
    cryptedpassword = pwd.getpwnam(username)[1]
    if cryptedpassword:
        if cryptedpassword in ['x','X','*']:
            try:
                cryptedpassword = spwd.getspnam(username)[1]
            except PermissionError:
                raise PermissionError("Program must be run on root for shadow password support!")
        password = getpass('Password: ', '*')
        if compare_digest(crypt.crypt(password,cryptedpassword), cryptedpassword):
            print('\33[92m' + "Login Successful!" + '\33[0m')
            raise SystemExit
        else:
            print('\33[91m' + "Invalid Credentials!" + '\33[0m')
            raise SystemExit
    else:
        print('\33[91m' + "Encrypted Password not detected!" + '\33[0m')
    
if __name__ == '__main__':
    login()