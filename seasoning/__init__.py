__version__ = '0.1.0'
__author__ = "Tristan Misja"

import ssl
import crypt
import getpass
import os
import spwd
import pwd
import termios
import tty
import pty
import hashlib
import secrets
import sys
import io
import subprocess
import socket
import time
import random

frequency_map = {"a": 0.0817,"b": 0.0150,"c": 0.0278,"d": 0.0425,"e": 0.1270,"f": 0.0223,"g": 0.0202,"h": 0.0609,"i": 0.0697,"j": 0.0015,"k": 0.0077,"l": 0.0403,"m": 0.0241,"n": 0.0675,"o": 0.0751,"p": 0.0193,"q": 0.0010,"r": 0.0599,"s": 0.0633,"t": 0.0906,"u": 0.0276,"v": 0.0098,"w": 0.0236,"x": 0.0015,"y": 0.0197,"z": 0.0007}

class AuthenticationError(PermissionError):
    pass

class SeasoningError(AuthenticationError):
    pass

def get_os():
    """
    Get the Operating System name.
    """
    system = sys.platform.lower()
    if system in ['win32','win64','win16','dos','cygwin','msys']:
        return "Windows"
    elif system in ['os2','os2emx']:
        return "OS/2"
    elif system == 'riscos':
        return "RISC OS"
    elif system == 'atheos':
        return "AtheOS"
    elif system in ['freebsd7','freebsd8','freebsdn']:
        return "FreeBSD"
    elif system == 'openbsd6':
        return "OpenBSD"
    elif system in ['linux','linux1','linux2','linux3']:
        return "Linux"
    elif system in ['darwin']:
        return "Macintosh"
    elif system in ['aix','aix5','aix7']:
        return "AIX"
    else:
        return "Unknown"

def get_os2():
    """
    Get the general system.
    """
    system = sys.platform.lower()
    if system in ['win32','win16','win64','dos','cygwin','msys','os2','os2emx']:
        return "Microsoft"
    elif system in ['riscos','openbsd6','linux','linux1','linux2','linux3','freebsd7','freebsd8','freebsdn','unix','posix']:
        return "POSIX"
    elif system == 'darwin':
        return 'Macintosh'
    else:
        return "Other"
    
def is_osfile(fp):
    f = fp.split('.')
    f = f[len(f)].lower()
    if f in ['iso','img','ima','dmg','partimg','wim','swm','esd','bac']:
        return True
    else:
        return False
    
def is_package_or_app(fp):
    f = fp.split('.')
    f = f[len(f)].lower()
    if f in ['deb','pkg','mpkg','rpm','tgz','msi','crx','apk','app','a','bac','8bf','bpl','com','dll','dol','elf','exe','ipa','jeff','jar','nlm','o','rll','s1es','so','vap','war','xbe','xap','xex','vbx','ocx','tlb']:
        return True
    else:
        return False
    
def is_keyfile(fp):
    f = fp.split('.')
    f = f[len(f)].lower()
    if f in ['key','pem','ssh','ppk','pub','crt','csr','cer','der','p7b','p7c','p12','pfx','bpw','kdb','kdbx']:
        return True
    else:
        return False
    
def is_audiofile(fp):
    f = fp.split('.')
    f = f[len(f)].lower()
    if f in ['ac3','amr','ra','rm','flac','la','pac','ape','ofr','ofs','off','rka','shn','tak','thd','tta','wv','wma','brstm','dts','dtshd','dysma','ast','aw','psf','mp1','mp2','mp3','spk','gsm','aac','mpc','vqf','ots','swa','vox','voc','dwd','smp','ogg','8svx','16svx','aiff','aif','aifc','au','bwf','cdda','wav','mid','midi','nsf','snd']:
        return True
    else:
        return False
    
def is_videofile(fp):
    f = fp.split('.')
    f = f[len(f)].lower()
    if f in ['asf','3gp','aaf','avchd','avi','bik','cam','collab','dsh','dvrms','flv','mp4','mpg','m1v','m2v','fla','flr','sol','m4v','mkv','wrap','mng','mov','mpeg','mpe','thp','mxf','roq','nsv','rm','svi','smi','smk','wmv','wtv','yuv','webm']:
        return True
    else:
        return False
    
def is_document(fp):
    f = fp.split('.')
    f = f[len(f)].lower()
    if f in ['0','1st','600','602','abw','acl','afp','ami','ans','asc','aww','ccf','csv','cwk','dbk','dita','doc','docm','docx','dot','dotx','dwd','egt','epub','ezw','fdx','ftm','ftx','gdoc','html','hwp','hwpml','log','lwp','mbp','md','rst','me','mcw','mobi','nb','nbp','neis','odm','odoc','odt','osheet','ott','omm','pages','pap','pdax','pdf','quox','rtf','rpt','sdw','se','stw','sxw','tex','txt','text','info','uni','uof','uoml','via','wpd','wps','wpt','wrd','wrf','wri','xhtml','xht','xhtm','htm','xml','xps']:
        return True
    else:
        return False
    
def gensalt(length=16):
    return os.urandom(length)

def genpepper(length=16):
    digits = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ:;,.?!@#$%^&*(){}<>'\"[]/|\`~-_=+"
    pepper = ''
    for _ in range(length):
        pepper += random.choice(random.sample(random.sample(digits,len(digits)),len(digits)))
    return pepper

# def roadmap_encrypt(string, key_list):
#     string = list(string)
#     out = b''
#     for num in range(len(string)):
#         key_num = key_list[num]
#         char_num = ord(string[num])
#         if key_num > char_num:
#             out += bytes(chr(key_num - char_num),'utf8')
#         else:
#             out += bytes(chr(char_num - key_num),'utf8')
#     return out

# def roadmap_decrypt(encrypted_bytes, key_list):
#     out = ''
#     var = []
#     encrypted_bytes = list(str(encrypted_bytes))
#     if len(encrypted_bytes) < len(key_list):
#         for num in range(len(encrypted_bytes)):
#             key_num = key_list[num]
#             char_num = ord(str(encrypted_bytes[num]))
#             if key_num > char_num:
#                 out += str(chr(char_num + key_num))
#             else:
#                 out += str(chr(key_num + char_num))
#     else:
#         for num in range(len(key_list)):
#             key_num = key_list[num]
#             char_num = ord(str(encrypted_bytes[num]))
#             if key_num > char_num:
#                 out += str(chr(char_num + key_num))
#             else:
#                 out += str(chr(key_num + char_num))
#     return out
    
def brute_force(verify_method, max_length=16):
    digits = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ:;,.?!@#$%^&*(){}<>'\"[]/|\`~-_=+"
    while 1:
        attempt = ''
        for _ in range(random.randint(1,max_length)):
            attempt += random.choice(random.sample(random.sample(digits,len(digits)),len(digits)))
        if verify_method(attempt).lower() in [1,'1',True,'true','correct']:
            return attempt
        else:
            continue
    
def season_data(data,pepper=genpepper(),salt=gensalt()):
    """
    Encrypts data using SHA256, with a salt and a pepper.
    """
    pepper, salt = str(pepper), str(salt)
    if len(pepper) != 16:
        return SeasoningError("Pepper must be 16 digits long!")
    if len(salt) != 16:
        return SeasoningError("Salt must be 16 digits long!")
    
    tmp1 = hashlib.sha256()
    tmp1.update(pepper.encode('latin1'))
    tmp1.update(data.encode('latin1'))
    hash_ = tmp1.hexdigest()
    del pepper
    tmp2 = hashlib.sha256()
    tmp2.update(salt.encode('latin1'))
    tmp2.update(hash_.encode('latin1'))
    hashed = tmp2.hexdigest()
    
    return hashed, salt

def season_data_crypt(data,pepper=genpepper(),salt=gensalt()):
    """
    WARNING: This is not secure. Use season_data() instead.
    """
    pepper, salt = str(pepper), str(salt)
    if len(pepper) != 16:
        return SeasoningError("Pepper must be 16 digits long!")
    if len(salt) != 16:
        return SeasoningError("Salt must be 16 digits long!")
    
    hashed = crypt.crypt(data,pepper)
    del pepper
    hashed = crypt.crypt(hashed,salt)
    
    return hashed, salt
    
def compare_seasoned(hashed, data, pepper, salt):
    pepper, salt = str(pepper), str(salt)
    
    tmp1 = hashlib.sha256()
    tmp1.update(pepper.encode('latin1'))
    tmp1.update(data.encode('latin1'))
    hash_ = tmp1.hexdigest()
    del pepper
    tmp2 = hashlib.sha256()
    tmp2.update(salt.encode('latin1'))
    tmp2.update(hash_.encode('latin1'))
    hashed_tmp = tmp2.hexdigest()
    
    if not hmac.compare_digest(hashed, hashed_tmp):
        return False
    else:
        return True

def getpass(prompt="Password: ", mask=''):
    if len(mask) not in [1,0]:
        raise ValueError("Mask can only be 1 digit long!")
    
    def getch():
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch
    if mask in ['', ' ']:
        getpass.getpass(prompt)
    else:
        enteredpassword = []
        sys.stdout.write(prompt)
        sys.stdout.flush()
        while 1:
            key = ord(getch())
            if key == 13:
                sys.stdout.write('\n')
                return ''.join(enteredpassword)
            elif key in [8, 127]:
                if len(enteredpassword) > 0:
                    sys.stdout.write('\b \b')
                    try:
                        curses.initscr()
                        curses.beep()
                        curses.endwin()
                    except:
                        pass
                    sys.stdout.flush()
                    enteredpassword = enteredpassword[:-1]
            elif 0 <= key <= 31:
                pass
            else:
                char = chr(key)
                sys.stdout.write(mask)
                sys.stdout.flush()
                enteredpassword.append(char)
    
def compare_seasoned_crypt(hashed, data, pepper, salt):
    pepper, salt = str(pepper), str(salt)
    
    if not hmac.compare_digest(hashed, crypt.crypt(crypt.crypt(data,pepper),salt)):
        return False
    else:
        return True
    
def secure_socket(sockobj, context=None, context_path=None):
    if context_path:
        ctx = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
        if type(context_path) in [__import__('pathlib').Path, __import__('pathlib').PurePath, __import__('pathlib').PosixPath, __import__('pathlib').PureWindowPath, __import__('pathlib').WindowPath, __import__('pathlib').PurePosixPath]:
            context_path = str(context_path)
        ctx.load_verify_locations(context_path)
        
        return ctx.wrap_socket(sockobj)
    elif context:
        return ctx.wrap_socket(sockobj)
    else:
        return ssl.generate_default_context().wrap_socket(sockobj)