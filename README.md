# Seasoning

Seasoning is a  Pure-Python, no dependencies package 
that allows users to encrypt data with salts and peppers,
as well as wrap [socket](docs.python.org/3/library/socket.html) objects, a improvement of [getpass.getpass()](docs.python.org/3/library/getpass.html#getpass()),
and more.

## Installation

You can install this project using ```pip```

    sudo python3 -m pip install Seasoning

or build it using ```git```

    git clone https://github.com/TristanMisja/Seasoning.git
    cd Seasoning
    sudo python3 setup.py install
    
This package only works on POSIX systems running Python 3.
  
## Documentation

Seasoning Data:

    import seasoning
    
    data = "PASSWORD"
    salt = "saltsaltsaltsalt"
    pepper = "pperpperpperpper"
    
    hashed = seasoning.season_data(data,pepper,salt)[0]
  
the result is:

    93543f4ce2b46de935a1f8e7bae4d1e03d872508d7e229d2a868d780bd19502f
  
  
This package also has an improved ```getpass.getpass()``` which has
a ```mask``` argument. Here is an example:

    import seasoning
    
    password = seasoning.getpass(prompt="Password: ", mask='*')
  
if ```mask``` is ```''```, it with call ```getpass.getpass()```.


Verifying seasoned data:
    
	import seasoning
	
	data = "pswd"                # Slightly different
	salt = "SaltSaltSaltSalt"    # than actual
	pepper = "peprpeprpeprpepr"  # credentials.
	
	hashed = seasoning.season_data("PASSWORD","pperpperpperpper","saltsaltsaltsalt")
	
	if not seasoning.compare_seasoned(hashed, data, pepper, salt):
	  raise seasoning.AuthenticationError("Incorrect Password and/or Pepper!")
	else:
	  print("Correct!")

## Other links:

[PyPi](https://pypi.org/project/Seasoning)

[BitBucket](https://bitbucket.org/TristanMisja/seasoning.git)

[Github](https://github.com/TristanMisja/Seasoning.git)
