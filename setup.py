import pathlib
from setuptools import setup, find_packages

HERE = pathlib.Path(__file__).parent

README = (HERE / "README.md").read_text()

# REQUIREMENTS = open('requirements.txt','r').read().split('\n')

setup(
    name="Seasoning",
    version="0.1.0",
    description="Encrypt objects with salt and pepper in Pure-Python, with no dependencies.",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://github.com/TristanMisja/Seasoning",
    download_url="https://github.com/TristanMisja/Seasoning",
    author="Tristan Misja",
    author_email="TristanMisja@gmail.com",
    maintainer="Tristan Misja",
    maintainer_email="TristanMisja@gmail.com",
    license="MIT",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Topic :: Security :: Cryptography",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: System :: Systems Administration :: Authentication/Directory",
        "Operating System :: POSIX",
        "Operating System :: Unix",
        "Intended Audience :: System Administrators",
        "Intended Audience :: Developers",
        "Development Status :: 2-Pre-Alpha",
        "Natural Language :: English"
    ],
    keywords="cryptography encryption hashing",
    packages=['seasoning'], # find_packages(),
    include_package_data=True,
    install_requires=[], # REQUIREMENTS,
    requires=[], # REQUIREMENTS,
    provides=["Seasoning"],
    zip_safe=False,
    entry_points={
        "console_scripts": [
            "seasoning=seasoning.__main__:main"
        ]# ,
        # "gui_scripts": [
        #     "seasoning-gui=seasoning.__main_gui__:main"
        # ]
    },
    project_urls={
        "Bug Tracker": "https://github.com/TristanMisja/Seasoning/issues",
        "Documentation": "https://pypi.org/project/Seasoning",
        "Source Code": "https://github.com/TristanMisja/Seasoning"
    }
)