sudo ssh-keygen -f /home/pi/.ssh/key -t rsa -b 4096

sudo openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout key.pem -out cert.pem

sudo openssl req -new -newkey rsa:2048 -nodes -keyout key.key -out key.csr

sudo openssl x509 -req -days 3650 -in key.csr -signkey key.key -out key.crt